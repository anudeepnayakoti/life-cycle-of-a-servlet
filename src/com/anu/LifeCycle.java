package com.anu;

import java.io.IOException;
import java.io.PrintWriter;
//import java.io.Writer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LifeCycle extends HttpServlet {
    public LifeCycle() {
		// TODO Auto-generated constructor stub
    	super();
    	System.out.println("This is inside the Constructor");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String firstName = req.getParameter("fname");
		String lastName = req.getParameter("lname");
		PrintWriter obj1 = resp.getWriter();
		obj1.println("Thank you! " + firstName + lastName + " for visiting our page.");

	}
	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		System.out.println("This is inside the Ini::++");//This is called only once per life cycle
	}
	
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.service(req, res);
		System.out.println("Service class");//This is called everytime a new request is made.
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
		System.out.println("App got shut down");//This will be called once per life-cycle and shuts the app.
	}
}
